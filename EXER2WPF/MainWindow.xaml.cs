﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EXER2WPF.WCF;
using System.Text.RegularExpressions;

namespace EXER2WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //beri nice comment

        ServiceClient sc = new ServiceClient();
        private string FN, LN, MN, StNum, Prog, Sect, Subj;
        private int Yr;

        private void txtbxMN_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^a-zA-Z]+");
        }

        private void txtbxLN_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^a-zA-Z]+");
        }

        private void txtbxProg_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^a-zA-Z]+");
        }

        private void txtbxSubj1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^a-zA-Z0-9_]+");
        }

        private void txtbxSect_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^a-zA-Z0-9_]+");
        }

        private void txtbxFN_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^a-zA-Z]+");
        }

        private void txtbxYR_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            txtbField.Clear();
            txtbxFN.Clear();
            txtbxLN.Clear();
            txtbxMN.Clear();
            txtbxProg.Clear();
            txtbxSect.Clear();
            txtbxSN.Clear();
            txtbxSubj1.Clear();
            txtbxYR.Clear();
            showFields();
        }

        private void btnFullN_Click(object sender, RoutedEventArgs e)
        {
            txtbField.Clear();
            txtbField.Text = sc.getFullName(FN, MN, LN).ToUpper();
        }

        private void btnProgYr_Click(object sender, RoutedEventArgs e)
        {
            txtbField.Clear();
            txtbField.Text = sc.getProgYear(Prog, Yr).ToUpper();
        }

        private void btnSubjSec_Click(object sender, RoutedEventArgs e)
        {
            txtbField.Clear();
            txtbField.Text = sc.getSubjSection(Subj, Sect).ToUpper();
        }

        private void btnStudentNum_Click(object sender, RoutedEventArgs e)
        {
            txtbField.Clear();
            txtbField.Text = sc.getStudentNum(StNum).ToUpper();
        }

        private void btnFullInf_Click(object sender, RoutedEventArgs e)
        {
            txtbField.Clear();
            txtbField.Text = sc.getFullName(FN, MN, LN).ToUpper() +"\n"+ sc.getStudentNum(StNum).ToUpper()+"\n"+
                sc.getProgYear(Prog, Yr).ToUpper()+"\n"+ sc.getSubjSection(Subj, Sect).ToUpper();
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            if(ddlOptions.SelectedIndex>-1)
            {
                getInfoNeeded(ddlOptions.SelectedIndex);
            }
            else
            {
                MessageBox.Show("SELECT AN INFORMATION TO OUTPUT", "NOTICE");
            }

        }

        private void getInfoNeeded(int i)
        {
            switch(i)
            {
                case 0:
                    txtbField.Clear();
                    txtbField.Text = sc.getFullName(FN, MN, LN).ToUpper();
                    break;
                case 1:
                    txtbField.Clear();
                    txtbField.Text = sc.getProgYear(Prog, Yr).ToUpper();
                    break;
                case 2:
                    txtbField.Clear();
                    txtbField.Text = sc.getSubjSection(Subj, Sect).ToUpper();
                    break;
                case 3:
                    txtbField.Clear();
                    txtbField.Text = sc.getStudentNum(StNum).ToUpper();
                    break;
                case 4:
                    txtbField.Clear();
                    txtbField.Text = sc.getFullName(FN, MN, LN).ToUpper() + "\n" + sc.getStudentNum(StNum).ToUpper() + "\n" +
                        sc.getProgYear(Prog, Yr).ToUpper() + "\n" + sc.getSubjSection(Subj, Sect).ToUpper();
                    break;
                default:
                    break;
            }
        }

        private void txtbxSN_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }



        public MainWindow()
        {
            InitializeComponent();
            showFields();
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if(checkFields())
            {
                hideAndGetStuff();
            }
            else
            {
                MessageBox.Show("All Fields are not filled up", "NOTICE");
            }

        }

        private bool checkFields()
        {
            bool allFieldNotEmpt = false;
            if(txtbxFN.Text.Length>0 && txtbxLN.Text.Length > 0 && txtbxMN.Text.Length > 0 &&
                txtbxProg.Text.Length > 0 && txtbxSect.Text.Length > 0 && txtbxSN.Text.Length > 0 &&
                txtbxSubj1.Text.Length > 0 && txtbxYR.Text.Length > 0)
            {
                allFieldNotEmpt = true;
            }


            return allFieldNotEmpt;
        }

        private void hideAndGetStuff()
        {
            FN = txtbxFN.Text;
            MN = txtbxMN.Text;
            LN = txtbxLN.Text;
            StNum = txtbxSN.Text;
            Prog = txtbxProg.Text;
            Sect = txtbxSect.Text;
            Subj = txtbxSubj1.Text;
            Yr = int.Parse(txtbxYR.Text);

            lbl1.Visibility = Visibility.Hidden;
            lbl2.Visibility = Visibility.Hidden;
            lbl3.Visibility = Visibility.Hidden;
            lbl4.Visibility = Visibility.Hidden;
            lbl5.Visibility = Visibility.Hidden;
            lbl6.Visibility = Visibility.Hidden;
            lbl7.Visibility = Visibility.Hidden;
            lbl8.Visibility = Visibility.Hidden;
            txtbxFN.Visibility = Visibility.Hidden;
            txtbxLN.Visibility = Visibility.Hidden;
            txtbxMN.Visibility = Visibility.Hidden;
            txtbxProg.Visibility = Visibility.Hidden;
            txtbxSect.Visibility = Visibility.Hidden;
            txtbxSN.Visibility = Visibility.Hidden;
            txtbxSubj1.Visibility = Visibility.Hidden;
            txtbxYR.Visibility = Visibility.Hidden;
            btnSubmit.Visibility = Visibility.Hidden;

            btnBack.Visibility = Visibility.Visible;
            btnGet.Visibility = Visibility.Visible;
            ddlOptions.Visibility = Visibility.Visible;
            txtbField.Visibility = Visibility.Visible;
        }

        private void showFields()
        {
            lbl1.Visibility = Visibility.Visible;
            lbl2.Visibility = Visibility.Visible;
            lbl3.Visibility = Visibility.Visible;
            lbl4.Visibility = Visibility.Visible;
            lbl5.Visibility = Visibility.Visible;
            lbl6.Visibility = Visibility.Visible;
            lbl7.Visibility = Visibility.Visible;
            lbl8.Visibility = Visibility.Visible;
            txtbxFN.Visibility = Visibility.Visible;
            txtbxLN.Visibility = Visibility.Visible;
            txtbxMN.Visibility = Visibility.Visible;
            txtbxProg.Visibility = Visibility.Visible;
            txtbxSect.Visibility = Visibility.Visible;
            txtbxSN.Visibility = Visibility.Visible;
            txtbxSubj1.Visibility = Visibility.Visible;
            txtbxYR.Visibility = Visibility.Visible;
            btnSubmit.Visibility = Visibility.Visible;

            btnGet.Visibility = Visibility.Hidden;
            btnBack.Visibility = Visibility.Hidden;
            ddlOptions.Visibility = Visibility.Hidden;
            txtbField.Visibility = Visibility.Hidden;

        }
    }
}
